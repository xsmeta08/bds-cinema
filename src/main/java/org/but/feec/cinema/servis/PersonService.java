package org.but.feec.cinema.servis;

import at.favre.lib.crypto.bcrypt.BCrypt;
import org.but.feec.cinema.api.PersonBasicView;
import org.but.feec.cinema.api.PersonCreateView;
import org.but.feec.cinema.api.PersonDetailView;
import org.but.feec.cinema.api.PersonEditView;
import org.but.feec.cinema.api.PersonFilterView;
import org.but.feec.cinema.api.PersonDeleteView;
import org.but.feec.cinema.data.PersonRepository;

import java.util.List;

public class PersonService {
    private PersonRepository personRepository;
    public PersonService(PersonRepository personRepository){
        this.personRepository = personRepository;
    }

    public List<PersonBasicView> getPersonsBasicView() {
        return personRepository.getPersonsBasicView();
    }

    public PersonDetailView getPersonDetailView(Long id) {
        return personRepository.findPersonDetailedView(id);
    }

    public PersonFilterView getPersonFilterView(Long id) { return personRepository.findPersonFilteredView(id); }

    public void editPerson(PersonEditView personEditView) {
        personRepository.editPerson(personEditView);
    }

    public void deletePerson(PersonDeleteView personDeleteView) { personRepository.deletePerson(personDeleteView); }


    public void createPerson(PersonCreateView personCreateView) {
        char[] originalPassword = personCreateView.getPwd();
        char[] hashedPassword = hashPassword(originalPassword);
        personCreateView.setPwd(hashedPassword);

        personRepository.createPerson(personCreateView);
    }

    private char[] hashPassword(char[] password) {
        return BCrypt.withDefaults().hashToChar(12, password);
    }


}
