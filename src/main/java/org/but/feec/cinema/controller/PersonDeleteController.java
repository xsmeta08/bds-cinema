package org.but.feec.cinema.controller;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.but.feec.cinema.api.PersonDeleteView;
import org.but.feec.cinema.api.PersonBasicView;
import org.but.feec.cinema.api.PersonEditView;
import org.but.feec.cinema.data.PersonRepository;
import org.but.feec.cinema.servis.PersonService;
import org.controlsfx.validation.ValidationSupport;
import org.controlsfx.validation.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;

public class PersonDeleteController {
    private static final Logger logger = LoggerFactory.getLogger(PersonDeleteController.class);

    @FXML
    public Button deletePersonButton;

    @FXML
    public TextField deletePersonId;

    @FXML
    public TextField deletePersonGivenName;

    @FXML
    public TextField deletePersonFamilyName;

    @FXML
    public TextField deletePersonMail;

    private PersonService personService;
    private PersonRepository personRepository;
    private ValidationSupport validation;
    public Stage stage;

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @FXML
    public void initialize() {
        personRepository = new PersonRepository();
        personService = new PersonService(personRepository);

        validation = new ValidationSupport();
        validation.registerValidator(deletePersonId, Validator.createEmptyValidator("The id must not be empty."));
        deletePersonId.setEditable(false);
        validation.registerValidator(deletePersonGivenName, Validator.createEmptyValidator(("The given name must not be empty.")));
        deletePersonGivenName.setEditable(false);
        validation.registerValidator(deletePersonFamilyName, Validator.createEmptyValidator("The family name must not be empty."));
        deletePersonFamilyName.setEditable(false);
        validation.registerValidator(deletePersonMail, Validator.createEmptyValidator("The mail must not be empty."));
        deletePersonMail.setEditable(false);


        deletePersonButton.disableProperty().bind(validation.invalidProperty());

        loadPersonsData();

        logger.info("PersonEditController initalized");
    }

    private void loadPersonsData(){
        Stage stage = this.stage;
        if (stage.getUserData() instanceof PersonBasicView){
            PersonBasicView personBasicView = (PersonBasicView) stage.getUserData();
            deletePersonId.setText(String.valueOf(personBasicView.getId()));
            deletePersonGivenName.setText(String.valueOf(personBasicView.getGivenName()));
            deletePersonFamilyName.setText(String.valueOf(personBasicView.getFamilyName()));
            deletePersonMail.setText(String.valueOf(personBasicView.getMail()));
        }
    }

    public void handleDeletePersonButton(ActionEvent event) {
        Long id = Long.valueOf(deletePersonId.getText());
        String givenName = deletePersonGivenName.getText();
        String mail = deletePersonMail.getText();
        String familyName = deletePersonFamilyName.getText();

        PersonDeleteView personDeleteView = new PersonDeleteView();
        personDeleteView.setId(id);
        personDeleteView.setGivenName(givenName);
        personDeleteView.setFamilyName(familyName);
        personDeleteView.setMail(mail);

        personService.deletePerson(personDeleteView);

        personEditedConfirmationDialog();
    }

    private void personEditedConfirmationDialog() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Person Deleted Confirmation");
        alert.setHeaderText("Your person was successfully deleted.");

        Timeline idlestage = new Timeline(new KeyFrame(Duration.seconds(3), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                alert.setResult(ButtonType.CANCEL);
                alert.hide();
            }
        }));
        idlestage.setCycleCount(1);
        idlestage.play();
        Optional<ButtonType> result = alert.showAndWait();
    }

}
