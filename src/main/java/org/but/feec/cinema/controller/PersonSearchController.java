package org.but.feec.cinema.controller;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.but.feec.cinema.api.PersonFilterView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PersonSearchController {

    private static final Logger logger = LoggerFactory.getLogger(PersonSearchController.class);

    @FXML
    public TextField searchPersonGivenName;

    @FXML
    public TextField searchPersonFamilyName;

    @FXML
    public TextField searchPersonMail;

    public Stage stage;

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @FXML
    public void initialize() {
        searchPersonGivenName.setEditable(false);
        searchPersonFamilyName.setEditable(false);
        searchPersonMail.setEditable(false);

        loadPersonsData();

        logger.info("PersonSearchController initialized");
    }

    private void loadPersonsData()
    {
        Stage stage = this.stage;
        if (stage.getUserData() instanceof PersonFilterView)
        {
            PersonFilterView personBasicView = (PersonFilterView) stage.getUserData();
            searchPersonGivenName.setText(String.valueOf(personBasicView.getGivenName()));
            searchPersonFamilyName.setText(String.valueOf(personBasicView.getFamilyName()));
            searchPersonMail.setText(String.valueOf(personBasicView.getMail()));

        }
    }


}
