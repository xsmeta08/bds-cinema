package org.but.feec.cinema.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import org.but.feec.cinema.App;
import org.but.feec.cinema.api.PersonBasicView;
import org.but.feec.cinema.api.PersonDetailView;
import org.but.feec.cinema.api.PersonFilterView;
import org.but.feec.cinema.data.PersonRepository;
import org.but.feec.cinema.exceptions.ExceptionHandler;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.*;
import org.but.feec.cinema.servis.PersonService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;

public class PersonsController {
    @FXML
    public Button sqlInjectionButton;

    @FXML
    public Button searchPersonButton;

    @FXML
    public Button addPersonButton;

    @FXML
    public Button refreshButton;

    @FXML
    public TextField searchId;

    @FXML
    private TableColumn<PersonBasicView, Long> personId;

    @FXML
    private TableColumn<PersonBasicView, String> personEmail;

    @FXML
    private TableColumn<PersonBasicView, String> personName;

    @FXML
    private TableView<PersonBasicView> systemPersonsTableView;

    private static final Logger logger = LoggerFactory.getLogger(PersonsController.class);

    public PersonsController() {
    }

    private PersonService personService;
    private PersonRepository personRepository;

    @FXML
    private void initialize(){
        personRepository = new PersonRepository();
        personService = new PersonService(personRepository);

        personId.setCellValueFactory(new PropertyValueFactory<PersonBasicView, Long>("id"));
        personEmail.setCellValueFactory(new PropertyValueFactory<PersonBasicView, String>("mail"));
        personName.setCellValueFactory(new PropertyValueFactory<PersonBasicView, String>("givenName"));

        ObservableList<PersonBasicView> observablePersonsList = initializePersonsData();
        systemPersonsTableView.setItems(observablePersonsList);

        systemPersonsTableView.getSortOrder().add(personId);

        initializeTableViewSelection();


        logger.info("PersonsController initialized");
    }

    private ObservableList<PersonBasicView> initializePersonsData() {
        List<PersonBasicView> persons = personService.getPersonsBasicView();
        return FXCollections.observableArrayList(persons);
    }


    public void handleExitMenuItem(ActionEvent event) {
        System.exit(0);
    }

    private void initializeTableViewSelection() {
        MenuItem edit = new MenuItem("Edit person");
        MenuItem detailedView = new MenuItem("Detailed person view");
        MenuItem delete = new MenuItem("Delete person");
        edit.setOnAction((ActionEvent event) -> {
            PersonBasicView personView = systemPersonsTableView.getSelectionModel().getSelectedItem();
            try {
                FXMLLoader fxmlLoader = new FXMLLoader();
                fxmlLoader.setLocation(App.class.getResource("fxml/PersonEdit.fxml"));
                Stage stage = new Stage();
                stage.setUserData(personView);
                stage.setTitle("BDS CINEMA");

                PersonEditController controller = new PersonEditController();
                controller.setStage(stage);
                fxmlLoader.setController(controller);

                Scene scene = new Scene(fxmlLoader.load(), 600, 500);

                stage.setScene(scene);

                stage.show();
            } catch (IOException ex) {
                ExceptionHandler.handleException(ex);
            }
        });

        detailedView.setOnAction((ActionEvent event) -> {
            PersonBasicView personView = systemPersonsTableView.getSelectionModel().getSelectedItem();
            try {
                FXMLLoader fxmlLoader = new FXMLLoader();
                fxmlLoader.setLocation(App.class.getResource("fxml/PersonDetailView.fxml"));
                Stage stage = new Stage();

                Long personId = personView.getId();
                PersonDetailView personDetailView = personService.getPersonDetailView(personId);

                stage.setUserData(personDetailView);
                stage.setTitle("BDS CINEMA");

                PersonDetailViewController controller = new PersonDetailViewController();
                controller.setStage(stage);
                fxmlLoader.setController(controller);

                Scene scene = new Scene(fxmlLoader.load(), 600, 500);

                stage.setScene(scene);

                stage.show();
            } catch (IOException ex) {
                ExceptionHandler.handleException(ex);
            }
        });

    delete.setOnAction((ActionEvent event) ->{
        PersonBasicView personView = systemPersonsTableView.getSelectionModel().getSelectedItem();
        try{
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(App.class.getResource("fxml/PersonDelete.fxml"));
            Stage stage = new Stage();
            stage.setUserData(personView);
            stage.setTitle("BDS CINEMA");

            PersonDeleteController controller = new PersonDeleteController();
            controller.setStage(stage);
            fxmlLoader.setController(controller);

            Scene scene = new Scene(fxmlLoader.load(), 600, 500);

            stage.setScene(scene);

            stage.show();
        }
        catch(IOException ex){
            ExceptionHandler.handleException(ex);
        }
    });


        ContextMenu menu = new ContextMenu();
        menu.getItems().add(edit);
        menu.getItems().addAll(detailedView);
        menu.getItems().add(delete);
        systemPersonsTableView.setContextMenu(menu);
    }

    public void handleRefreshButton(ActionEvent actionEvent) {
        ObservableList<PersonBasicView> observablePersonsList = initializePersonsData();
        systemPersonsTableView.setItems(observablePersonsList);
        systemPersonsTableView.refresh();
        systemPersonsTableView.sort();
    }

    public void handleAddPersonButton(ActionEvent actionEvent) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(App.class.getResource("fxml/PersonCreate.fxml"));
            Scene scene = new Scene(fxmlLoader.load(), 600, 500);
            Stage stage = new Stage();
            stage.setTitle("BDS Cinema");
            stage.setScene(scene);
            stage.show();
        } catch (IOException ex) {
            ExceptionHandler.handleException(ex);
        }
    }

    public void handleSearchPersonButton(ActionEvent actionEvent) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(App.class.getResource("fxml/PersonSearch.fxml"));
            Stage stage = new Stage();

            Long personId = Long.valueOf(searchId.getText());
            PersonFilterView personFilterView = personService.getPersonFilterView(personId);

            stage.setUserData(personFilterView);
            stage.setTitle("BDS CINEMA");

            PersonSearchController controller = new PersonSearchController();
            controller.setStage(stage);
            fxmlLoader.setController(controller);

            Scene scene = new Scene(fxmlLoader.load(), 600, 500);

            stage.setScene(scene);

            stage.show();
        } catch (IOException ex) {
            ExceptionHandler.handleException(ex);
        }

    }

    public void handleSqlInjectionButton(ActionEvent actionEvent) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(App.class.getResource("fxml/SqlInjection.fxml"));
            Scene scene = new Scene(fxmlLoader.load(), 600, 400);
            Stage stage = new Stage();
            stage.setTitle("BDS CINEMA");
            stage.setScene(scene);

            stage.show();
        } catch (IOException ex) {
            ExceptionHandler.handleException(ex);
        }
    }

}
