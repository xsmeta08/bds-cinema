package org.but.feec.cinema.controller;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.but.feec.cinema.api.PersonDetailView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PersonDetailViewController {

    private static final Logger logger = LoggerFactory.getLogger(PersonDetailViewController.class);

    @FXML
    private TextField idTextField;
    @FXML
    private TextField givenNameTextField;
    @FXML
    private TextField familyNameTextField;
    @FXML
    private TextField mailTextField;
    @FXML
    private TextField membershipTypeTextField;

    public Stage stage;

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @FXML
    public void initialize() {
        idTextField.setEditable(false);
        givenNameTextField.setEditable(false);
        familyNameTextField.setEditable(false);
        mailTextField.setEditable(false);
        membershipTypeTextField.setEditable(false);


        loadPersonsData();
        logger.info("PersonsDetailViewController initialized");

    }

    private void loadPersonsData()
    {
        Stage stage = this.stage;
        if (stage.getUserData() instanceof PersonDetailView)
        {
            PersonDetailView personBasicView = (PersonDetailView) stage.getUserData();
            idTextField.setText(String.valueOf(personBasicView.getId()));
            givenNameTextField.setText(String.valueOf(personBasicView.getGivenName()));
            familyNameTextField.setText(String.valueOf(personBasicView.getFamilyName()));
            mailTextField.setText(String.valueOf(personBasicView.getMail()));
            membershipTypeTextField.setText(String.valueOf(personBasicView.getMembershipType()));

        }
    }

}
