package org.but.feec.cinema.controller;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.but.feec.cinema.api.PersonBasicView;
import org.but.feec.cinema.api.PersonEditView;
import org.but.feec.cinema.data.PersonRepository;
import org.but.feec.cinema.servis.PersonService;
import org.controlsfx.validation.ValidationSupport;
import org.controlsfx.validation.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;

public class PersonEditController {
    private static final Logger logger = LoggerFactory.getLogger(PersonEditController.class);

    @FXML
    public Button editPersonButton;
    @FXML
    public TextField idTextField;
    @FXML
    public TextField givenNameTextField;
    @FXML
    public TextField familyNameTextField;
    @FXML
    public TextField mailTextField;

    private PersonService personService;
    private PersonRepository personRepository;
    private ValidationSupport validation;
    public Stage stage;

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @FXML
    public void initialize() {
        personRepository = new PersonRepository();
        personService = new PersonService(personRepository);

        validation = new ValidationSupport();
        validation.registerValidator(idTextField, Validator.createEmptyValidator("The id must not be empty."));
        idTextField.setEditable(false);
        validation.registerValidator(givenNameTextField, Validator.createEmptyValidator(("The given name must not be empty.")));
        validation.registerValidator(familyNameTextField, Validator.createEmptyValidator("The family name must not be empty."));
        validation.registerValidator(mailTextField, Validator.createEmptyValidator("The mail must not be empty."));


        editPersonButton.disableProperty().bind(validation.invalidProperty());

        loadPersonsData();

        logger.info("PersonEditController initalized");
    }

    private void loadPersonsData(){
        Stage stage = this.stage;
        if (stage.getUserData() instanceof PersonBasicView){
            PersonBasicView personBasicView = (PersonBasicView) stage.getUserData();
            idTextField.setText(String.valueOf(personBasicView.getId()));
            givenNameTextField.setText(String.valueOf(personBasicView.getGivenName()));
            familyNameTextField.setText(String.valueOf(personBasicView.getFamilyName()));
            mailTextField.setText(String.valueOf(personBasicView.getMail()));
        }
    }

    public void handleEditPersonButton(ActionEvent event) {
        Long id = Long.valueOf(idTextField.getText());
        String givenName = givenNameTextField.getText();
        String mail = mailTextField.getText();
        String familyName = familyNameTextField.getText();

        PersonEditView personEditView = new PersonEditView();
        personEditView.setId(id);
        personEditView.setGivenName(givenName);
        personEditView.setFamilyName(familyName);
        personEditView.setMail(mail);

        personService.editPerson(personEditView);

        personEditedConfirmationDialog();
    }

    private void personEditedConfirmationDialog() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Person Edited Confirmation");
        alert.setHeaderText("Your person was successfully edited.");

        Timeline idlestage = new Timeline(new KeyFrame(Duration.seconds(3), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                alert.setResult(ButtonType.CANCEL);
                alert.hide();
            }
        }));
        idlestage.setCycleCount(1);
        idlestage.play();
        Optional<ButtonType> result = alert.showAndWait();
    }


}
