package org.but.feec.cinema.api;

public class PersonEditView {
    private Long id;
    private String givenName;
    private String familyName;
    private String mail;


    public Long getId() { return id; }

    public void setId(Long id) { this.id=id; }

    public String getGivenName() { return givenName; }

    public void setGivenName(String givenName) { this.givenName = givenName; }

    public String getFamilyName() { return familyName; }

    public void setFamilyName(String familyName) { this.familyName = familyName; }

    public String getMail() { return mail; }

    public void setMail(String mail) { this.mail = mail; }



    @Override
    public String toString() {
        return "PersonEditView{" +
                "email='" + mail + '\'' +
                ", givenName='" + givenName + '\'' +
                ", familyName='" + familyName + '\'' +
                '}';
    }

}
