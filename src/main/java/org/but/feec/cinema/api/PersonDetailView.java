package org.but.feec.cinema.api;

import javafx.beans.property.LongProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class PersonDetailView {
    private LongProperty id = new SimpleLongProperty();
    private StringProperty mail = new SimpleStringProperty();
    private StringProperty givenName = new SimpleStringProperty();
    private StringProperty familyName = new SimpleStringProperty();
    private StringProperty membershipType = new SimpleStringProperty();

    public Long getId() { return idProperty().get(); }

    public void setId(Long id) { this.idProperty().setValue(id); }

    public String getMail() { return mailProperty().get(); }

    public void setMail(String mail) { this.mailProperty().setValue(mail); }

    public String getGivenName() { return givenNameProperty().get(); }

    public void setGivenName(String givenName) { this.givenNameProperty().setValue(givenName); }

    public String getFamilyName() { return familyNameProperty().get(); }

    public void setFamilyName(String familyName) { this.familyNameProperty().setValue(familyName);}

    public String getMembershipType() { return membershipTypeProperty().get(); }

    public void setMembershipType(String membershipType) { this.membershipTypeProperty().setValue(membershipType);}


    public LongProperty idProperty() {
        return id;
    }

    public StringProperty mailProperty() {
        return mail;
    }

    public StringProperty givenNameProperty() {
        return givenName;
    }

    public StringProperty familyNameProperty() { return familyName; }

    public StringProperty membershipTypeProperty() { return membershipType; }
}
