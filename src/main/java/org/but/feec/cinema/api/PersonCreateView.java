package org.but.feec.cinema.api;

import java.util.Arrays;

public class PersonCreateView {
    private String mail;
    private String givenName;
    private String familyName;
    private char[] pwd;

    public String getMail() { return mail; }

    public void setMail(String mail) { this.mail = mail; }

    public String getGivenName() { return givenName; }

    public void setGivenName(String givenName) { this.givenName = givenName; }

    public String getFamilyName() { return familyName; }

    public void setFamilyName(String familyName) { this.familyName = familyName; }

    public char[] getPwd() { return pwd; }

    public void setPwd(char[] pwd) { this.pwd = pwd; }

    @Override
    public String toString() {
        return "PersonCreateView{" +
                "mail='" + mail + '\'' +
                ", given name='" + givenName + '\'' +
                ", family name='" + familyName + '\'' +
                ", pwd=" + Arrays.toString(pwd) +
                '}';
    }
}
